<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca_ES">
<context>
    <name>GameView</name>
    <message>
        <source>Install</source>
        <translation type="obsolete">Instal·lar</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Sí</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancel·lar</translation>
    </message>
    <message>
        <source>Play</source>
        <translation type="obsolete">Jugar</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="obsolete">Descripcció</translation>
    </message>
    <message>
        <source>Releases</source>
        <translation type="obsolete">Publicacions</translation>
    </message>
    <message>
        <source>Version %1</source>
        <translation type="obsolete">Versió %1</translation>
    </message>
    <message>
        <source>Developer</source>
        <translation type="obsolete">Desenvolupador</translation>
    </message>
    <message>
        <source>License</source>
        <translation type="obsolete">Llicència</translation>
    </message>
    <message>
        <source>Links</source>
        <translation type="obsolete">Enllaços</translation>
    </message>
    <message>
        <source>Homepage</source>
        <translation type="obsolete">Pàgina principal</translation>
    </message>
    <message>
        <source>Bug Tracker</source>
        <translation type="obsolete">Seguidor d&apos;errades</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Ajuda</translation>
    </message>
    <message>
        <source>FAQ</source>
        <translation type="obsolete">FAQ</translation>
    </message>
    <message>
        <source>Donate</source>
        <translation type="obsolete">Fer un donatiu</translation>
    </message>
    <message>
        <source>Translation</source>
        <translation type="obsolete">Traducció</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="obsolete">Desconegut</translation>
    </message>
    <message>
        <source>Manifest</source>
        <translation type="obsolete">Manifest</translation>
    </message>
</context>
<context>
    <name>LibraryGridView</name>
    <message>
        <location filename="../LibraryGridView.qml" line="24"/>
        <source>Search %L1 Games...</source>
        <translation type="unfinished">Cerca %L1 jocs...</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="82"/>
        <source>All (%L1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="83"/>
        <source>Installed (%L1)</source>
        <translation type="unfinished">Instal·lats</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="84"/>
        <source>Recent (%L1)</source>
        <translation type="unfinished">Recents</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="85"/>
        <source>Has Updates (%L1)</source>
        <translation type="unfinished">Conté actualitzacions</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="86"/>
        <source>Processing (%L1)</source>
        <translation type="unfinished">Processant</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="167"/>
        <source>Play</source>
        <translation type="unfinished">Jugar</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="174"/>
        <source>View In Store</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibraryListView</name>
    <message>
        <location filename="../LibraryListView.qml" line="23"/>
        <source>Search %L1 Games...</source>
        <translation type="unfinished">Cerca %L1 jocs...</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="79"/>
        <source>All (%L1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="80"/>
        <source>Installed (%L1)</source>
        <translation type="unfinished">Instal·lats</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="81"/>
        <source>Recent (%L1)</source>
        <translation type="unfinished">Recents</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="82"/>
        <source>Has Updates (%L1)</source>
        <translation type="unfinished">Conté actualitzacions</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="83"/>
        <source>Processing (%L1)</source>
        <translation type="unfinished">Processant</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="179"/>
        <source>New</source>
        <translation type="unfinished">Nou</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="198"/>
        <source>Nothing seems to be here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="303"/>
        <source>Install</source>
        <translation type="unfinished">Instal·lar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="318"/>
        <source>Download Size:	%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="325"/>
        <source>Installed Size:	%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="332"/>
        <source>Install Game?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="363"/>
        <source>Play</source>
        <translation type="unfinished">Jugar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="363"/>
        <source>In-Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="372"/>
        <source>Update</source>
        <translation type="unfinished">Actualitzar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="375"/>
        <source>Uninstall</source>
        <translation type="unfinished">Desinstal·lar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="396"/>
        <source>Are you sure?</source>
        <translation type="unfinished">Estàs segur?</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="647"/>
        <source>Description</source>
        <translation type="unfinished">Descripcció</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="652"/>
        <source>No description available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="676"/>
        <source>Similar Games</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="342"/>
        <location filename="../LibraryListView.qml" line="406"/>
        <source>Yes</source>
        <translation type="unfinished">Sí</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="345"/>
        <location filename="../LibraryListView.qml" line="409"/>
        <location filename="../LibraryListView.qml" line="424"/>
        <location filename="../LibraryListView.qml" line="478"/>
        <source>Cancel</source>
        <translation type="unfinished">Cancel·lar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="445"/>
        <source>Resolve Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="457"/>
        <source>Clear error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="462"/>
        <source>Mark as installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="469"/>
        <source>Mark as uninstalled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="704"/>
        <source>Releases</source>
        <translation type="unfinished">Publicacions</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="709"/>
        <source>No release information available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="733"/>
        <source>Version %1</source>
        <translation type="unfinished">Versió %1</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="750"/>
        <source>No release description available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="766"/>
        <source>Anti-Features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="778"/>
        <source>This game requires NonFree assets.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="805"/>
        <source>Developer</source>
        <translation type="unfinished">Desenvolupador</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="821"/>
        <source>License</source>
        <translation type="unfinished">Llicència</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="837"/>
        <source>Links</source>
        <translation type="unfinished">Enllaços</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="867"/>
        <source>Homepage</source>
        <translation type="unfinished">Pàgina principal</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="869"/>
        <source>Bug Tracker</source>
        <translation type="unfinished">Seguidor d&apos;errades</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="871"/>
        <source>Help</source>
        <translation type="unfinished">Ajuda</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="873"/>
        <source>FAQ</source>
        <translation type="unfinished">FAQ</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="876"/>
        <source>Donate</source>
        <translation type="unfinished">Fer un donatiu</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="878"/>
        <source>Translation</source>
        <translation type="unfinished">Traducció</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="880"/>
        <source>Unknown</source>
        <translation type="unfinished">Desconegut</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="882"/>
        <source>Manifest</source>
        <translation type="unfinished">Manifest</translation>
    </message>
</context>
<context>
    <name>LibraryView</name>
    <message>
        <source>Search %L1 Games...</source>
        <translation type="vanished">Cerca %L1 jocs...</translation>
    </message>
    <message>
        <source>Library</source>
        <translation type="vanished">Biblioteca</translation>
    </message>
    <message>
        <source>⋮</source>
        <translation type="vanished">⋮</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Paràmetres</translation>
    </message>
    <message>
        <source>Check For Updates</source>
        <translatorcomment>Shortened the translation a bit so that it fits</translatorcomment>
        <translation type="vanished">Comprova actualitzacions</translation>
    </message>
    <message>
        <source>Update All</source>
        <translation type="vanished">Actualitza-ho tot</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Surt</translation>
    </message>
    <message>
        <source>You have operations pending.</source>
        <translation type="vanished">Tens operacions pendents.</translation>
    </message>
    <message>
        <source>Close Anyway</source>
        <translation type="vanished">Tanca igualment</translation>
    </message>
    <message>
        <source>All Games (%L1)</source>
        <translation type="vanished">Tots els jocs (%L1)</translation>
    </message>
    <message>
        <source>Installed (%L1)</source>
        <translation type="vanished">Instal·lats (%L1)</translation>
    </message>
    <message>
        <source>Recent (%L1)</source>
        <translation type="vanished">Recents (%L1)</translation>
    </message>
    <message>
        <source>New (%L1)</source>
        <translation type="vanished">Nous (%L1)</translation>
    </message>
    <message>
        <source>Has Updates (%L1)</source>
        <translation type="vanished">Amb actualitzacions (%L1)</translation>
    </message>
    <message>
        <source>Processing (%L1)</source>
        <translation type="vanished">Processant (%L1)</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="vanished">Nou</translation>
    </message>
    <message>
        <source>Install</source>
        <translation type="vanished">Instal·la</translation>
    </message>
    <message>
        <source>Play</source>
        <translation type="vanished">Juga-hi</translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="vanished">Actualitza</translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation type="vanished">Desinstal·la</translation>
    </message>
    <message>
        <source>Are you sure?</source>
        <translation type="vanished">Estàs segur?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">Sí</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Cancel·la</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="vanished">Descripció</translation>
    </message>
    <message>
        <source>Releases</source>
        <translation type="vanished">Llançaments</translation>
    </message>
    <message>
        <source>Version %1</source>
        <translation type="vanished">Versió %1</translation>
    </message>
    <message>
        <source>Developer</source>
        <translation type="vanished">Desenvolupador</translation>
    </message>
    <message>
        <source>License</source>
        <translation type="vanished">Llicència</translation>
    </message>
    <message>
        <source>Links</source>
        <translation type="vanished">Enllaços</translation>
    </message>
    <message>
        <source>Homepage</source>
        <translation type="vanished">Pàgina principal</translation>
    </message>
    <message>
        <source>Bug Tracker</source>
        <translation type="vanished">Seguidor d&apos;errades</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Ajuda</translation>
    </message>
    <message>
        <source>FAQ</source>
        <translation type="vanished">PMF</translation>
    </message>
    <message>
        <source>Donate</source>
        <translation type="vanished">Fes un donatiu</translation>
    </message>
    <message>
        <source>Translation</source>
        <translation type="vanished">Traducció</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="vanished">Desconegut</translation>
    </message>
    <message>
        <source>Manifest</source>
        <translation type="vanished">Manifest</translation>
    </message>
    <message>
        <source>Categories</source>
        <translation type="vanished">Categories</translation>
    </message>
    <message>
        <source>Installed successfully.</source>
        <translation type="vanished">S&apos;ha instal·lat correctament.</translation>
    </message>
    <message>
        <source>Uninstalled successfully.</source>
        <translation type="vanished">S&apos;ha desinstal·lat correctament.</translation>
    </message>
    <message>
        <source>Updated successfully.</source>
        <translation type="vanished">S&apos;ha actualitzat correctament.</translation>
    </message>
    <message>
        <source>An error occurred.</source>
        <translation type="vanished">S&apos;ha produït un error.</translation>
    </message>
</context>
<context>
    <name>NavigationBar</name>
    <message>
        <location filename="../NavigationBar.qml" line="18"/>
        <source>Library</source>
        <translation type="unfinished">Biblioteca</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="29"/>
        <source>Settings</source>
        <translation type="unfinished">Paràmetres</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="50"/>
        <source>Show games in a list view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="60"/>
        <source>Show games in a grid view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="69"/>
        <source>Reset settings to defaults.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="81"/>
        <source>Check For Updates</source>
        <translation type="unfinished">Comprova actualitzacions</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="85"/>
        <source>Update All</source>
        <translation type="unfinished">Actualitza-ho tot</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="89"/>
        <source>Exit</source>
        <translation type="unfinished">Sortir</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="104"/>
        <source>You have operations pending.</source>
        <translation type="unfinished">Tens operacions pendents.</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="110"/>
        <source>Close Anyway</source>
        <translation type="unfinished">Tanca igualment</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="116"/>
        <source>Cancel</source>
        <translation type="unfinished">Cancel·lar</translation>
    </message>
</context>
<context>
    <name>SettingsView</name>
    <message>
        <source>‹</source>
        <translation type="vanished">‹</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Paràmetres</translation>
    </message>
    <message>
        <source>⋮</source>
        <translation type="vanished">⋮</translation>
    </message>
    <message>
        <source>Reset All</source>
        <translation type="vanished">Restableix-ho tot</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Surt</translation>
    </message>
    <message>
        <source>Show Tray Icon</source>
        <translation type="vanished">Mostra una icona a la safata del sistema</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="28"/>
        <source>Always Show Logs</source>
        <translation>Mostra sempre els registres</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="35"/>
        <source>Notifications Enabled</source>
        <translation>Notificacions habilitades</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="46"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="69"/>
        <source>Reset Database</source>
        <translation>Reinicialitza la base de dades</translation>
    </message>
</context>
</TS>
