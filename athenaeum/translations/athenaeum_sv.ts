<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>LibraryGridView</name>
    <message>
        <location filename="../LibraryGridView.qml" line="24"/>
        <source>Search %L1 Games...</source>
        <translation>Sök %L1 spel...</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="82"/>
        <source>All (%L1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="83"/>
        <source>Installed (%L1)</source>
        <translation type="unfinished">Installerade (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="84"/>
        <source>Recent (%L1)</source>
        <translation type="unfinished">Senaste (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="85"/>
        <source>Has Updates (%L1)</source>
        <translation type="unfinished">Har uppdateringar (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="86"/>
        <source>Processing (%L1)</source>
        <translation type="unfinished">Bearbetar (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="167"/>
        <source>Play</source>
        <translation type="unfinished">Spela</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="174"/>
        <source>View In Store</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Library</source>
        <translation type="vanished">Bibliotek</translation>
    </message>
    <message>
        <source>⋮</source>
        <translation type="vanished">⋮</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Inställningar</translation>
    </message>
    <message>
        <source>Check For Updates</source>
        <translation type="vanished">Sök efter uppdateringar</translation>
    </message>
    <message>
        <source>Update All</source>
        <translation type="vanished">Uppdatera alla</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Avsluta</translation>
    </message>
    <message>
        <source>You have operations pending.</source>
        <translation type="vanished">Du har väntande åtgärder.</translation>
    </message>
</context>
<context>
    <name>LibraryListView</name>
    <message>
        <source>Close Anyway</source>
        <translation type="vanished">Stäng ändå</translation>
    </message>
    <message>
        <source>All Games (%L1)</source>
        <translation type="vanished">Alla spel (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="80"/>
        <source>Installed (%L1)</source>
        <translation>Installerade (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="81"/>
        <source>Recent (%L1)</source>
        <translation>Senaste (%L1)</translation>
    </message>
    <message>
        <source>New (%L1)</source>
        <translation type="vanished">Nytt (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="82"/>
        <source>Has Updates (%L1)</source>
        <translation>Har uppdateringar (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="83"/>
        <source>Processing (%L1)</source>
        <translation>Bearbetar (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="179"/>
        <source>New</source>
        <translation>Nya</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="198"/>
        <source>Nothing seems to be here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="303"/>
        <source>Install</source>
        <translation>Installera</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="318"/>
        <source>Download Size:	%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="325"/>
        <source>Installed Size:	%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="332"/>
        <source>Install Game?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="363"/>
        <source>Play</source>
        <translation>Spela</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="363"/>
        <source>In-Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="372"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="375"/>
        <source>Uninstall</source>
        <translation>Avinstallera</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="396"/>
        <source>Are you sure?</source>
        <translation>Är du säker?</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="647"/>
        <source>Description</source>
        <translation type="unfinished">Beskrivning</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="652"/>
        <source>No description available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="676"/>
        <source>Similar Games</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="342"/>
        <location filename="../LibraryListView.qml" line="406"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="23"/>
        <source>Search %L1 Games...</source>
        <translation type="unfinished">Sök %L1 spel...</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="79"/>
        <source>All (%L1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="345"/>
        <location filename="../LibraryListView.qml" line="409"/>
        <location filename="../LibraryListView.qml" line="424"/>
        <location filename="../LibraryListView.qml" line="478"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="445"/>
        <source>Resolve Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="457"/>
        <source>Clear error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="462"/>
        <source>Mark as installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="469"/>
        <source>Mark as uninstalled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="704"/>
        <source>Releases</source>
        <translation>Utgåvor</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="709"/>
        <source>No release information available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="733"/>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="750"/>
        <source>No release description available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="766"/>
        <source>Anti-Features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="778"/>
        <source>This game requires NonFree assets.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="805"/>
        <source>Developer</source>
        <translation>Utvecklare</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="821"/>
        <source>License</source>
        <translation>Licens</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="837"/>
        <source>Links</source>
        <translation>Länkar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="867"/>
        <source>Homepage</source>
        <translation>Hemsida</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="869"/>
        <source>Bug Tracker</source>
        <translation>Felsökare</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="871"/>
        <source>Help</source>
        <translation>Hjälp</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="873"/>
        <source>FAQ</source>
        <translation>Frågor och svar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="876"/>
        <source>Donate</source>
        <translation>Donera</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="878"/>
        <source>Translation</source>
        <translation>Översättning</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="880"/>
        <source>Unknown</source>
        <translation>Okänd</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="882"/>
        <source>Manifest</source>
        <translation>Manifest</translation>
    </message>
</context>
<context>
    <name>NavigationBar</name>
    <message>
        <source>Categories</source>
        <translation type="vanished">Kategorier</translation>
    </message>
    <message>
        <source>Installed successfully.</source>
        <translation type="vanished">Installerat framgångsrikt.</translation>
    </message>
    <message>
        <source>Uninstalled successfully.</source>
        <translation type="vanished">Avinstallerat framgångsrikt.</translation>
    </message>
    <message>
        <source>Updated successfully.</source>
        <translation type="vanished">Uppdateringen lyckades.</translation>
    </message>
    <message>
        <source>An error occurred.</source>
        <translation type="vanished">Ett fel inträffade.</translation>
    </message>
    <message>
        <source>‹</source>
        <translation type="vanished">‹</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="18"/>
        <source>Library</source>
        <translation type="unfinished">Bibliotek</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="29"/>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="50"/>
        <source>Show games in a list view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="60"/>
        <source>Show games in a grid view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="69"/>
        <source>Reset settings to defaults.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="81"/>
        <source>Check For Updates</source>
        <translation type="unfinished">Sök efter uppdateringar</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="85"/>
        <source>Update All</source>
        <translation type="unfinished">Uppdatera alla</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="104"/>
        <source>You have operations pending.</source>
        <translation type="unfinished">Du har väntande åtgärder.</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="110"/>
        <source>Close Anyway</source>
        <translation type="unfinished">Stäng ändå</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="116"/>
        <source>Cancel</source>
        <translation type="unfinished">Avbryt</translation>
    </message>
    <message>
        <source>⋮</source>
        <translation type="vanished">⋮</translation>
    </message>
    <message>
        <source>Reset All</source>
        <translation type="vanished">Återställ alla</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="89"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <source>Show Tray Icon</source>
        <translation type="vanished">Visa aktivitetsikon</translation>
    </message>
</context>
<context>
    <name>SettingsView</name>
    <message>
        <location filename="../SettingsView.qml" line="28"/>
        <source>Always Show Logs</source>
        <translation>Visa alltid loggar</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="35"/>
        <source>Notifications Enabled</source>
        <translation>Aviseringar aktiverade</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="46"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="69"/>
        <source>Reset Database</source>
        <translation>Återställ databas</translation>
    </message>
</context>
</TS>
