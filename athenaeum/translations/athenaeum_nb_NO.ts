<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb_NO">
<context>
    <name>GameView</name>
    <message>
        <source>Install</source>
        <translation type="obsolete">Installer</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Ja</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Avbryt</translation>
    </message>
    <message>
        <source>Play</source>
        <translation type="obsolete">Spill</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="obsolete">Beskrivelse</translation>
    </message>
    <message>
        <source>Releases</source>
        <translation type="obsolete">Utgivelser</translation>
    </message>
    <message>
        <source>Version %1</source>
        <translation type="obsolete">Versjon %1</translation>
    </message>
    <message>
        <source>Developer</source>
        <translation type="obsolete">Utvikler</translation>
    </message>
    <message>
        <source>License</source>
        <translation type="obsolete">LIsens</translation>
    </message>
    <message>
        <source>Links</source>
        <translation type="obsolete">Lenker</translation>
    </message>
    <message>
        <source>Homepage</source>
        <translation type="obsolete">Hjemmeside</translation>
    </message>
    <message>
        <source>Bug Tracker</source>
        <translation type="obsolete">Feilsporer</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Hjelp</translation>
    </message>
    <message>
        <source>FAQ</source>
        <translation type="obsolete">O-S-S</translation>
    </message>
    <message>
        <source>Donate</source>
        <translation type="obsolete">Doner</translation>
    </message>
    <message>
        <source>Translation</source>
        <translation type="obsolete">Oversettelse</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="obsolete">Ukjent</translation>
    </message>
    <message>
        <source>Manifest</source>
        <translation type="obsolete">Manifest</translation>
    </message>
</context>
<context>
    <name>LibraryGridView</name>
    <message>
        <location filename="../LibraryGridView.qml" line="24"/>
        <source>Search %L1 Games...</source>
        <translation type="unfinished">Søk i %1L1 spill…</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="82"/>
        <source>All (%L1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="83"/>
        <source>Installed (%L1)</source>
        <translation type="unfinished">Installert (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="84"/>
        <source>Recent (%L1)</source>
        <translation type="unfinished">Nylige (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="85"/>
        <source>Has Updates (%L1)</source>
        <translation type="unfinished">Har oppdateringer (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="86"/>
        <source>Processing (%L1)</source>
        <translation type="unfinished">Behandler (%1L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="167"/>
        <source>Play</source>
        <translation type="unfinished">Spill</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="174"/>
        <source>View In Store</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Library</source>
        <translation type="vanished">Bibliotek</translation>
    </message>
</context>
<context>
    <name>LibraryListView</name>
    <message>
        <source>⋮</source>
        <translation type="obsolete">⋮</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Innstillinger</translation>
    </message>
    <message>
        <source>Check For Updates</source>
        <translation type="vanished">Se etter oppdateringer</translation>
    </message>
    <message>
        <source>Update All</source>
        <translation type="vanished">Oppdater alle</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Avslutt</translation>
    </message>
    <message>
        <source>You have operations pending.</source>
        <translation type="obsolete">Du har operasjoner under utførelse.</translation>
    </message>
    <message>
        <source>Close Anyway</source>
        <translation type="vanished">Lukk uansett</translation>
    </message>
    <message>
        <source>All Games (%L1)</source>
        <translation type="vanished">Samtlige spill (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="80"/>
        <source>Installed (%L1)</source>
        <translation>Installert (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="81"/>
        <source>Recent (%L1)</source>
        <translation>Nylige (%L1)</translation>
    </message>
    <message>
        <source>New (%L1)</source>
        <translation type="vanished">Nye (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="23"/>
        <source>Search %L1 Games...</source>
        <translation type="unfinished">Søk i %1L1 spill…</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="79"/>
        <source>All (%L1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="82"/>
        <source>Has Updates (%L1)</source>
        <translation>Har oppdateringer (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="83"/>
        <source>Processing (%L1)</source>
        <translation>Behandler (%1L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="179"/>
        <source>New</source>
        <translation>Nye</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="198"/>
        <source>Nothing seems to be here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="303"/>
        <source>Install</source>
        <translation>Installer</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="318"/>
        <source>Download Size:	%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="325"/>
        <source>Installed Size:	%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="332"/>
        <source>Install Game?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="342"/>
        <location filename="../LibraryListView.qml" line="406"/>
        <source>Yes</source>
        <translation type="unfinished">Ja</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="345"/>
        <location filename="../LibraryListView.qml" line="409"/>
        <location filename="../LibraryListView.qml" line="424"/>
        <location filename="../LibraryListView.qml" line="478"/>
        <source>Cancel</source>
        <translation type="unfinished">Avbryt</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="363"/>
        <source>Play</source>
        <translation>Spill</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="363"/>
        <source>In-Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="372"/>
        <source>Update</source>
        <translation type="unfinished">Oppdater</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="375"/>
        <source>Uninstall</source>
        <translation type="unfinished">Avinstaller</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="396"/>
        <source>Are you sure?</source>
        <translation type="unfinished">Er du sikker?</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="445"/>
        <source>Resolve Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="457"/>
        <source>Clear error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="462"/>
        <source>Mark as installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="469"/>
        <source>Mark as uninstalled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="647"/>
        <source>Description</source>
        <translation type="unfinished">Beskrivelse</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="652"/>
        <source>No description available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="676"/>
        <source>Similar Games</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="704"/>
        <source>Releases</source>
        <translation type="unfinished">Utgivelser</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="709"/>
        <source>No release information available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="733"/>
        <source>Version %1</source>
        <translation type="unfinished">Versjon %1</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="750"/>
        <source>No release description available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="766"/>
        <source>Anti-Features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="778"/>
        <source>This game requires NonFree assets.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="805"/>
        <source>Developer</source>
        <translation type="unfinished">Utvikler</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="821"/>
        <source>License</source>
        <translation type="unfinished">LIsens</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="837"/>
        <source>Links</source>
        <translation type="unfinished">Lenker</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="867"/>
        <source>Homepage</source>
        <translation type="unfinished">Hjemmeside</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="869"/>
        <source>Bug Tracker</source>
        <translation type="unfinished">Feilsporer</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="871"/>
        <source>Help</source>
        <translation type="unfinished">Hjelp</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="873"/>
        <source>FAQ</source>
        <translation type="unfinished">O-S-S</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="876"/>
        <source>Donate</source>
        <translation type="unfinished">Doner</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="878"/>
        <source>Translation</source>
        <translation type="unfinished">Oversettelse</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="880"/>
        <source>Unknown</source>
        <translation type="unfinished">Ukjent</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="882"/>
        <source>Manifest</source>
        <translation type="unfinished">Manifest</translation>
    </message>
</context>
<context>
    <name>LibraryView</name>
    <message>
        <source>Search %L1 Games...</source>
        <translation type="obsolete">Søk i %1L1 spill…</translation>
    </message>
    <message>
        <source>Library</source>
        <translation type="vanished">Bibliotek</translation>
    </message>
    <message>
        <source>⋮</source>
        <translation type="obsolete">⋮</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Innstillinger</translation>
    </message>
    <message>
        <source>Check For Updates</source>
        <translation type="vanished">Se etter oppdateringer</translation>
    </message>
    <message>
        <source>Update All</source>
        <translation type="vanished">Oppdater alle</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Avslutt</translation>
    </message>
    <message>
        <source>All Games (%L1)</source>
        <translation type="vanished">Samtlige spill (%L1)</translation>
    </message>
    <message>
        <source>Installed (%L1)</source>
        <translation type="vanished">Installert (%L1)</translation>
    </message>
    <message>
        <source>Recent (%L1)</source>
        <translation type="vanished">Nylige (%L1)</translation>
    </message>
    <message>
        <source>New (%L1)</source>
        <translation type="vanished">Nye (%L1)</translation>
    </message>
    <message>
        <source>Has Updates (%L1)</source>
        <translation type="vanished">Har oppdateringer (%L1)</translation>
    </message>
    <message>
        <source>Processing (%L1)</source>
        <translation type="vanished">Behandler (%1L1)</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="vanished">Nye</translation>
    </message>
    <message>
        <source>Install</source>
        <translation type="vanished">Installer</translation>
    </message>
    <message>
        <source>Play</source>
        <translation type="vanished">Spill</translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="vanished">Oppdater</translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation type="vanished">Avinstaller</translation>
    </message>
    <message>
        <source>Are you sure?</source>
        <translation type="vanished">Er du sikker?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">Ja</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Avbryt</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="vanished">Beskrivelse</translation>
    </message>
    <message>
        <source>Releases</source>
        <translation type="vanished">Utgivelser</translation>
    </message>
    <message>
        <source>Version %1</source>
        <translation type="vanished">Versjon %1</translation>
    </message>
    <message>
        <source>Developer</source>
        <translation type="vanished">Utvikler</translation>
    </message>
    <message>
        <source>License</source>
        <translation type="vanished">LIsens</translation>
    </message>
    <message>
        <source>Links</source>
        <translation type="vanished">Lenker</translation>
    </message>
    <message>
        <source>Homepage</source>
        <translation type="vanished">Hjemmeside</translation>
    </message>
    <message>
        <source>Bug Tracker</source>
        <translation type="vanished">Feilsporer</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Hjelp</translation>
    </message>
    <message>
        <source>FAQ</source>
        <translation type="vanished">O-S-S</translation>
    </message>
    <message>
        <source>Donate</source>
        <translation type="vanished">Doner</translation>
    </message>
    <message>
        <source>Translation</source>
        <translation type="vanished">Oversettelse</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="vanished">Ukjent</translation>
    </message>
    <message>
        <source>Manifest</source>
        <translation type="obsolete">Manifest</translation>
    </message>
    <message>
        <source>Categories</source>
        <translation type="vanished">Kategorier</translation>
    </message>
    <message>
        <source>Installed successfully.</source>
        <translation type="obsolete">Installert.</translation>
    </message>
    <message>
        <source>Uninstalled successfully.</source>
        <translation type="obsolete">Avinstallert.</translation>
    </message>
    <message>
        <source>Updated successfully.</source>
        <translation type="vanished">Oppdatert.</translation>
    </message>
    <message>
        <source>An error occurred.</source>
        <translation type="vanished">En feil oppstid.</translation>
    </message>
</context>
<context>
    <name>NavigationBar</name>
    <message>
        <location filename="../NavigationBar.qml" line="18"/>
        <source>Library</source>
        <translation type="unfinished">Bibliotek</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="29"/>
        <source>Settings</source>
        <translation type="unfinished">Innstillinger</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="50"/>
        <source>Show games in a list view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="60"/>
        <source>Show games in a grid view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="69"/>
        <source>Reset settings to defaults.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="81"/>
        <source>Check For Updates</source>
        <translation type="unfinished">Se etter oppdateringer</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="85"/>
        <source>Update All</source>
        <translation type="unfinished">Oppdater alle</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="89"/>
        <source>Exit</source>
        <translation type="unfinished">Avslutt</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="104"/>
        <source>You have operations pending.</source>
        <translation type="unfinished">Du har operasjoner under utførelse.</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="110"/>
        <source>Close Anyway</source>
        <translation type="unfinished">Lukk uansett</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="116"/>
        <source>Cancel</source>
        <translation type="unfinished">Avbryt</translation>
    </message>
</context>
<context>
    <name>SettingsView</name>
    <message>
        <source>‹</source>
        <translation type="vanished">‹</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Innstillinger</translation>
    </message>
    <message>
        <source>⋮</source>
        <translation type="obsolete">⋮</translation>
    </message>
    <message>
        <source>Reset All</source>
        <translation type="vanished">Tilbakestill alt</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Avslutt</translation>
    </message>
    <message>
        <source>Show Tray Icon</source>
        <translation type="vanished">Vis systemkurvsikon</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="28"/>
        <source>Always Show Logs</source>
        <translation>Alltid vis loggføring</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="35"/>
        <source>Notifications Enabled</source>
        <translation>Merknader påskrudd</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="46"/>
        <source>Theme</source>
        <translation>Drakt</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="69"/>
        <source>Reset Database</source>
        <translation>Tilbakestill database</translation>
    </message>
</context>
</TS>
