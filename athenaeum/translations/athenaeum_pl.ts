<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>LibraryGridView</name>
    <message>
        <location filename="../LibraryGridView.qml" line="24"/>
        <source>Search %L1 Games...</source>
        <translation>Szukaj %L1 według nazwy...</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="82"/>
        <source>All (%L1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="83"/>
        <source>Installed (%L1)</source>
        <translation type="unfinished">Zainstalowane (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="84"/>
        <source>Recent (%L1)</source>
        <translation type="unfinished">Ostatnio zagrane (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="85"/>
        <source>Has Updates (%L1)</source>
        <translation type="unfinished">Wymagające aktualizacji (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="86"/>
        <source>Processing (%L1)</source>
        <translation type="unfinished">Przetwarzanie (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="167"/>
        <source>Play</source>
        <translation type="unfinished">Graj</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="174"/>
        <source>View In Store</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Library</source>
        <translation type="vanished">Biblioteka</translation>
    </message>
    <message>
        <source>⋮</source>
        <translation type="vanished">⋮</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Ustawienia</translation>
    </message>
    <message>
        <source>Check For Updates</source>
        <translation type="vanished">Sprawdź aktualizacje</translation>
    </message>
    <message>
        <source>Update All</source>
        <translation type="vanished">Aktualizuj wszystkie</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Wyjście</translation>
    </message>
    <message>
        <source>You have operations pending.</source>
        <translation type="vanished">Oczekiwanie na wykonanie operacji.</translation>
    </message>
    <message>
        <source>Close Anyway</source>
        <translation type="vanished">Zamknij mimo to</translation>
    </message>
</context>
<context>
    <name>LibraryListView</name>
    <message>
        <location filename="../LibraryListView.qml" line="345"/>
        <location filename="../LibraryListView.qml" line="409"/>
        <location filename="../LibraryListView.qml" line="424"/>
        <location filename="../LibraryListView.qml" line="478"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
    <message>
        <source>All Games (%L1)</source>
        <translation type="vanished">Wszystkie gry (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="80"/>
        <source>Installed (%L1)</source>
        <translation>Zainstalowane (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="81"/>
        <source>Recent (%L1)</source>
        <translation>Ostatnio zagrane (%L1)</translation>
    </message>
    <message>
        <source>New (%L1)</source>
        <translation type="vanished">Nowe (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="82"/>
        <source>Has Updates (%L1)</source>
        <translation>Wymagające aktualizacji (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="83"/>
        <source>Processing (%L1)</source>
        <translation>Przetwarzanie (%L1)</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="179"/>
        <source>New</source>
        <translation>Nowe</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="198"/>
        <source>Nothing seems to be here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="303"/>
        <source>Install</source>
        <translation>Zainstaluj</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="318"/>
        <source>Download Size:	%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="325"/>
        <source>Installed Size:	%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="332"/>
        <source>Install Game?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="363"/>
        <source>Play</source>
        <translation>Graj</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="363"/>
        <source>In-Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="372"/>
        <source>Update</source>
        <translation>Aktualizacja</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="375"/>
        <source>Uninstall</source>
        <translation>Odinstaluj</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="396"/>
        <source>Are you sure?</source>
        <translation>Czy jesteś pewien?</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="647"/>
        <source>Description</source>
        <translation type="unfinished">Opis</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="652"/>
        <source>No description available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="676"/>
        <source>Similar Games</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="342"/>
        <location filename="../LibraryListView.qml" line="406"/>
        <source>Yes</source>
        <translation>Tak</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="23"/>
        <source>Search %L1 Games...</source>
        <translation type="unfinished">Szukaj %L1 według nazwy...</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="79"/>
        <source>All (%L1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="445"/>
        <source>Resolve Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="457"/>
        <source>Clear error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="462"/>
        <source>Mark as installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="469"/>
        <source>Mark as uninstalled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="704"/>
        <source>Releases</source>
        <translation>Wersje</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="709"/>
        <source>No release information available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="733"/>
        <source>Version %1</source>
        <translation>Wersja %1</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="750"/>
        <source>No release description available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="766"/>
        <source>Anti-Features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="778"/>
        <source>This game requires NonFree assets.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="805"/>
        <source>Developer</source>
        <translation>Wydawca</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="821"/>
        <source>License</source>
        <translation>Licencja</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="837"/>
        <source>Links</source>
        <translation>Linki</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="867"/>
        <source>Homepage</source>
        <translation>Strona główna</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="869"/>
        <source>Bug Tracker</source>
        <translation>Śledzenie błędów</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="871"/>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="873"/>
        <source>FAQ</source>
        <translation>FAQ</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="876"/>
        <source>Donate</source>
        <translation>Wspomóż nas</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="878"/>
        <source>Translation</source>
        <translation>Tłumaczenie</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="880"/>
        <source>Unknown</source>
        <translation>Nieznany</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="882"/>
        <source>Manifest</source>
        <translation type="unfinished">Manifest</translation>
    </message>
</context>
<context>
    <name>NavigationBar</name>
    <message>
        <source>Categories</source>
        <translation type="vanished">Kategorie</translation>
    </message>
    <message>
        <source>Installed successfully.</source>
        <translation type="vanished">Zainstalowano pomyślnie.</translation>
    </message>
    <message>
        <source>Uninstalled successfully.</source>
        <translation type="vanished">Odinstalowano pomyślnie.</translation>
    </message>
    <message>
        <source>Updated successfully.</source>
        <translation type="vanished">Zaktualizowano pomyślnie.</translation>
    </message>
    <message>
        <source>An error occurred.</source>
        <translation type="vanished">Wystąpił błąd.</translation>
    </message>
    <message>
        <source>‹</source>
        <translation type="vanished">‹</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="18"/>
        <source>Library</source>
        <translation type="unfinished">Biblioteka</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="29"/>
        <source>Settings</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="50"/>
        <source>Show games in a list view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="60"/>
        <source>Show games in a grid view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="69"/>
        <source>Reset settings to defaults.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="81"/>
        <source>Check For Updates</source>
        <translation type="unfinished">Sprawdź aktualizacje</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="85"/>
        <source>Update All</source>
        <translation type="unfinished">Aktualizuj wszystkie</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="104"/>
        <source>You have operations pending.</source>
        <translation type="unfinished">Oczekiwanie na wykonanie operacji.</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="110"/>
        <source>Close Anyway</source>
        <translation type="unfinished">Zamknij mimo to</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="116"/>
        <source>Cancel</source>
        <translation type="unfinished">Anuluj</translation>
    </message>
    <message>
        <source>⋮</source>
        <translation type="vanished">⋮</translation>
    </message>
    <message>
        <source>Reset All</source>
        <translation type="vanished">Zresetuj wszystko</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="89"/>
        <source>Exit</source>
        <translation>Wyjście</translation>
    </message>
    <message>
        <source>Show Tray Icon</source>
        <translation type="vanished">Pokaż ikonę programu</translation>
    </message>
</context>
<context>
    <name>SettingsView</name>
    <message>
        <location filename="../SettingsView.qml" line="28"/>
        <source>Always Show Logs</source>
        <translation>Zawsze pokazuj logi</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="35"/>
        <source>Notifications Enabled</source>
        <translation>Włącz powiadomienia</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="46"/>
        <source>Theme</source>
        <translation>Motyw</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="69"/>
        <source>Reset Database</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
