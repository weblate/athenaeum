import QtQuick 2.9
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.3
import Athenaeum 1.0

Popup {
    property string source: ''

    onAboutToShow: {
        fullscreenImage.source = source
    }
    anchors.centerIn: Overlay.overlay
    width: Overlay.overlay.width
    height: Overlay.overlay.height
    dim: true
    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
    MouseArea {
        anchors.fill: parent
        onClicked: {
            close()
        }
    }
    background: Image {
        id: fullscreenImage
        fillMode: Image.PreserveAspectFit
        anchors.centerIn: parent
        width: sourceSize.width > parent.width ? parent.width : sourceSize.width
        height: parent.height
    }
}
